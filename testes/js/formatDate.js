// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD
  
    //Separa a data pela barra
    var dataAPI = userDate.split('/');

     // Testa se mês e dia estão com duas casas preenchidas
    if(dataAPI[0].length == 1)
    {
  	  dataAPI[0] ='0'+dataAPI[0];
    } 
    
    else if (dataAPI[1].length == 1)
    {
  	  dataAPI[1] ='0'+dataAPI[1];
    } 
  
    //Juntando os valores na string para retornar e retornando
    return dataAPI[2] + dataAPI[0] + dataAPI[1];
}